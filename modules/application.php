<?php
class application {
	private static $mycolor = NULL;
	private static $ar_COLOR_N = NULL;
	public static function START() {
		self::$mycolor = array("#C88C13" , "#5AC600" , "#06538F" , "#008686");
		self::$ar_COLOR_N = count(self::$mycolor) - 1;
	}
	public static function GETcolor() {
		return self::$mycolor[rand(0 , self::$ar_COLOR_N)];
	}
	public function include_component($ar_PARAMS) {
		if(file_exists("components/".$ar_PARAMS["NAME"]."/template.php"))
			include("components/".$ar_PARAMS["NAME"]."/template.php");
		else
			return false;
		return true;
	}
	public function include_js($file) {
		if(file_exists($file))
			echo "<script type='text/javascript' src='".$file."'></script>";
		else if(file_exists("view/js/".$file))
			echo "<script type='text/javascript' src='view/js/".$file."'></script>";
		else
			return false;
		return true;	
	}
	
	public static $lang = array(
          'en'=>array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'),
          'ru'=>array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'),
          'ua'=>array('а','б','в','г','ґ','д','е','є','ж','з','и','і','ї','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ь','ю','я')
    );
	
}

?>