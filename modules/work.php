﻿<?php 
	class WORK {
		static public function ImageCreate($src) {
			$ar_Result = get_name($src);
			if($ar_Result["TYPE"] == "jpg") {
				$Result = imagecreatefromjpeg($src);
			}else if($ar_Result["TYPE"] == "png") {
				$Result = imagecreatefrompng($src);
			}
			return (isset($Result)) ? $Result : false;
		}
		static public function PutFile($ar_PARAMS , $decode) {
			$result = get_name($ar_PARAMS["FILE"]);
			if(in_array($result["TYPE"] , $ar_PARAMS["TYPES"])) {
				if(!(file_exists($ar_PARAMS["URL"]."/".$result["NAME"].".".$result["TYPE"]))) {
					file_put_contents($ar_PARAMS["URL"]."/".$result["NAME"].".".$result["TYPE"] ,
								  ($decode) ? base64_decode($ar_PARAMS["VALUE"]) : $ar_PARAMS["VALUE"]);	
					return array("NAME" => $result["NAME"] , "TYPE" => $result["TYPE"]);
				}
			}
			return false;
		}
		
		static public function SaveJSON($dir , $value) {
			$getter = json_encode($value);
			$bool = file_put_contents($dir , $getter);
			return $bool;
		}
		
		static public function GetJSON($dir , $value) {
			$getter = file_get_contents($dir); 	
			$ar_Result = json_decode($getter);
			return ($ar_Result) ? $ar_Result : false;
		} 
			
	}
?>