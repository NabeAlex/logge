<?php
class route {
	private $start_path;
	private $path;
	private $template;
	
	private $private_path;
	
	function __construct() {
		$this->start_path = isset($_GET["route"]) ? $_GET["route"] : "";
		$path = $this->work($this->start_path);
		$this->private_path = array("admin");
	}
	
	function work($start_path) {
		$bit = splitFile($start_path);	
		$this->path = ($start_path == "") ? "controllers/main/main.php" : "controllers/".$bit."/".$bit.".php";	
		if(!(file_exists($this->path)))
			$this->path = "controllers/404/404.php";
			
		$this->template = 
			($start_path == "") ? "controllers/main/main-template.php" : "controllers/".$bit."/".$bit."-template.php";	
		if(!(file_exists($this->template)))
			$this->template = "controllers/404/404-template.php";
	}
	
	public function get() {
		$result = array("path" => $this->path , "template" => $this->template);
		return $result;	
	}
	
}
?>