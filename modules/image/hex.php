﻿<?php
	class hex {
		private $hex = NULL;
		public function __construct($hex, $type) {
			$this->hex = $this->hex($hex , $type);	
		}
		function hex($image_src , $type) {
		 if(is_file($image_src))	 {
			$size=getimagesize($image_src);
			$image = imagecreatefrompng($image_src);
			if($image == false) return false;						
			$zone = imagecreate(20 , 20);
			imagecopyresized($zone,$image,0,0,0,0,20,20,$size[0],$size[1]);
			$map = array(); 
			$all = 0;
			for($x=0;$x<20;$x++) {
				for($y=0;$y<20;$y++)
				{
					$color=imagecolorat($zone,$x,$y);
					$color=imagecolorsforindex($zone,$color);
					$map[$x][$y] = 0.212671 * $color['red'] + 0.715160 * $color['green'] + 0.072169 * $color['blue'];
					$all += $map[$x][$y];
					}
				}
				$all /= 400;
				$result = array();
			
				for($x=0; $x<20; $x++)
				{
					for($y=0; $y<20; $y++)
					{
						$el = ($map[$x][$y]==0 ? '0' /*BLACK*/ : round(2*($map[$x][$y]>$all?$map[$x][$y]/$all:-1*$all/$map[$x][$y])));
						$result[] = ($x<10?$x:chr($x+97)).($y<10?$y:chr($y+97)).$el;
					}
				}
				return $result;
			}else{
				echo "MOME";
				return false;
			}
		}
			
			public function get() {
				return $this->hex;	
			}
	}
?>