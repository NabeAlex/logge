﻿<?php
	class compare {
		private $result = array();
		public static $path = NULL;
		public function __construct($this_hex) {
			if(compare::$path != NULL) $this->compare($this_hex);	
		}
		public function compare($main_hex) {	
			$paths = glob(compare::$path);
			foreach($paths as $path) {
				$result = 0;
				$string = file_get_contents($path);
				$hexes = json_decode($string);
				foreach($hexes as $bit)
				{
     					if(in_array($bit, $main_hex))
     					{
         					$result++;
     					}
  				}
				$preview = $result / 400;
				if($preview > 0) {
  		  			$this->result[] = array("NAME" => get_name($path) ,
										"VALUE" => $preview );
				}
			}
			
	
		}
		public function get() {
			return $this->result;	
		}
	}
?>