
function file_(data) {
	var t = data.split("\\");
	//encodeURIComponent(
	return t[t.length - 1];	
}

function read(file) {
	var doc = document,
		canvas = doc.createElement("canvas");
		ctx = canvas.getContext('2d');
		image = new Image();
		topic = id("own_image");
		out = id('output');
		info = id("info");
		out.innerHTML = "";
		$("#own_image").slideUp();
		$("#info").slideUp();
		
		id("dir_file").innerHTML = (file_($("#load").val())=="") ? "Файл не выбран" : file_($("#load").val());
		
		image.onload = function() {
			$("#site").css("opacity" , "0.3")
			canvas.width = image.width;
			canvas.height = image.height;
        	
			ctx.drawImage(image , 0 , 0); 
			var data = canvas.toDataURL();
			
			out.appendChild(canvas);
	
		    canvas.style.width = "100%";
			
			topic.style.width = (image.width <= 300)  ? image.width + "px"  : "40%";
			info.innerHTML = "<p style='font-size:60%;'>Width:" + image.width + "<br>Height:" + image.height + "</p>";
			
			$("#own_image").slideDown();
			$("#info").slideDown();
			
			$.ajax({ type:"POST", 
			url:"ajax/image.php" , 
					  data:{ "img" : data , "file" : file_($("#load").val()) },
					  
					  success: function(data) {
						  	$("#site").css("opacity" , "1.0")
							if(data=="error") {
								alert("Что-то пошло не так! Возможно, такая картинка уже есть.");
								return;
							}
								$("#name-logo").val("");
								id("result").innerHTML = data;
					  }
			});
			
		}
	
		image.src = URL.createObjectURL( file );
		
		
}
