// JavaScript Document
	function id(i){return document.getElementById(i)}

	function to(data) {
		window.location = (data.length != 0) ? "main?company=" + data : "main";
	}
$(document).ready(function(e) {
	
  	$("#clear").click(function(e) {
        $("#company").val("");
		$("#company").focus();
    });
	var KEY_CODE = {
		LEFT:37,
		UP:38,
		RIGHT:39,
		DOWN:40,
		ENTER:13
	}
	function handler(event) {
		switch(event.keyCode) {
			case KEY_CODE.LEFT:
				//setTimeout( function() { window.location = "main"; } , 0);
				break;
			case KEY_CODE.ENTER:
				break;	
		}
	}
	
	var button_go = $(".gogogo");
	var txt = $("#company");
	var t = document.getElementById("hint");
	var txt_ = document.getElementById("company");

	txt_.addEventListener("keydown" , downup , false)
	txt_.addEventListener("focus" , focus_ , false);
	txt_.onblur = function() {
  		delete_hints();
	}

	window.addEventListener("keydown" , handler , false);
	
	var selected = -1;
	var word_user = "";
	var can_delete = false;
	var textbox = 20;
	function focus_() {
		if(txt.val().toString()!="") {
			window.scrollTo(0 , textbox);
			data = "localhost";
			$("#hint").fadeIn(0);
			ajax();
		}else{
			can_delete=false;
			t.innerHTML = "";	
			$("#hint").fadeOut(500);		
		};
	}
	
	function delete_hints() {
		if(can_delete) {
			$("#hint").fadeOut(500);
			can_delete = false;
		}
	}
	function downup(event) {
				
		if(event.keyCode==174) return;
		window.scrollTo(0 , textbox);	
		if(txt.val().toString()!="") {
		switch(event.keyCode) {
			case KEY_CODE.DOWN:
				if(can_delete == false) {
					button_go.focus();	
					return;
				}else{
				var will_remove = document.getElementsByClassName("hint-select");
				if(will_remove.length > 0) will_remove[0].className = "hints";
				var mass = document.getElementsByClassName("hints");
				var max_ = mass.length - 1;
				if(selected == -1)
					word_user = txt_.value;
				selected++;
				if(selected > max_) 
					selected = max_;
				txt.val(mass[selected].innerHTML);
				mass[selected].className = "hint-select";
				}
				break;
			case KEY_CODE.UP:
				if(can_delete == false) {
					button_go.focus();	
					return;
				}else{
				var will_remove = document.getElementsByClassName("hint-select");
				if(will_remove.length > 0) will_remove[0].className = "hints";
				var mass = document.getElementsByClassName("hints");
				selected -= (selected == -1) ? 0 : 1;
				if(selected < 0) {
					txt_.value = word_user;
					
					setTimeout(function() { txt_.setSelectionRange(txt_.value.length, txt_.value.length) } , 0);
					break;
				}
				txt.val(mass[selected].innerHTML);
				mass[selected].className = "hint-select";
				
				setTimeout(function() { txt_.setSelectionRange(txt_.value.length, txt_.value.length) } , 0);	
				}
				break;
			case KEY_CODE.ENTER:
				var texting = document.getElementById("company");
				to(texting.value);
				break;
			
		}
		return;
		}
			switch(event.keyCode) {
				case KEY_CODE.DOWN:
					
					if(can_delete == false) {
						button_go.focus();	
						return;
					}break;
				case KEY_CODE.UP:
					if(can_delete == false) {
						button_go.focus();	
						return;
					}break;
			}
		}
	
	
	
	txt.bind("keyup" , function(event) {
		if((KEY_CODE.UP == event.keyCode)||(KEY_CODE.DOWN == event.keyCode)||(KEY_CODE.ENTER== event.keyCode)||(KEY_CODE.LEFT== event.keyCode)||(KEY_CODE.RIGHT== event.keyCode))
			return
		if(txt.val().toString()!="") {
		data = "localhost";
		$("#hint").fadeIn(0);
		ajax();
			
		}else{
			can_delete = false;
			t.innerHTML = "";	
			$("#hint").fadeOut(500);		
		}
	});
	
	button_go.click(function(e) {
		var texting = document.getElementById("company");
		to(texting.value);
    });
  	function ajax() {
	$.ajax({
			type : "GET",
			url: "ajax/hints.php?word=" + txt.val().toString() ,
			success: function(data) {
				selected = -1;
				if(data!="error") {
					setTimeout(function(){can_delete = true;} , 0);
					t.innerHTML = data;
					return;
				}else
					setTimeout(function(){can_delete = false;} , 0);	
				t.innerHTML = "";	
			}
	});
	}
});		