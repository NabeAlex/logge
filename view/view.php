<?
	global $ar_paths; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Machine</title>
<link type="text/css" href="view/style/style.css" rel="stylesheet"></link>
<link type="text/css" href="view/style/other.css" rel="stylesheet"></link>

<script type="text/javascript" src="view/js/jquery.js"></script>
<script type="text/javascript" src="view/js/push.js"></script>
<script type="text/javascript" src="view/js/main.js"></script>
<script type="text/javascript" src="view/js/jquery.tubular.1.0.js"></script>

<? if(($_GET['route'] == "main")||($_GET['route'] == ""))
	$APPLICATION->include_js("search.js");
?>
<? if(($_GET['route'] == "loadim"))
	$APPLICATION->include_js("load.js");
?>
<? if(($_GET['route'] == "byim"))
	$APPLICATION->include_js("work_image.js");
?>
</head>

<body>
	<div id="site">
		<div id="header">
        	<div class="holder">
            <div id="menu">
            
            <div id="apps">
            	
            </div>	
                <div id="test">
                   <a href='main' class='slide-link'><div class='point' style='margin-top:0px' id="search-im"></div></a>
                   <a href='main' class='slide-link'><div class='point' style='margin-top:25px'>Поиск</div></a>
                   <a href='byim' class='slide-link'><div class='point' style='margin-top:50px'>Похоже</div></a>
                   <a href='loadim' class='slide-link'><div class='point' style='margin-top:75px' id="load-im"></div></a>
                   <a href='loadim' class='slide-link'><div class='point' style='margin-top:100px'>Выложить</div></a>
                </div>
            
        	
            </div>
            </div>
        </div>
        <?php
        	include_once($ar_paths["template"]);
        ?>
        <div id="content">
        <div class="holder">
        <div class="menu-links">
        	<ul style="text-align:left;"> 
                <li><a href="main" class="links">By Text</a></li> 
                <li><a href="byim" class="links">By Image</a></li>  
                <li><div id="more-add"><a style="cursor:pointer;" id="button-more-add" class="links">More</a></div></li>  
                <li><a  style="cursor:pointer;" class="links" id="about">About</a></li>  
            </ul>
       </div>
       
        	<?php
				require_once($ar_paths["path"]);
			?>
		</div>
        </div>
        
        
        <div id="footer">
        		
            	<?php //$APPLICATION->include_component(array("NAME" => "image")); ?>
      
        </div>
        
    	<div id="footer-under">
        	<div class="holder">
        		<?php
					$APPLICATION->include_component(array("NAME" => "article"));
				?>
            </div>
        </div>
        
        
    	<div id="under-footer-under">
            	<?php $APPLICATION->include_component(array("NAME" => "footer")); ?>
    	</div>
    </div>
    
    
</body>
</html>