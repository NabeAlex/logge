// JavaScript Document
function gamemap(canvas_) {

	var draw = 	canvas_.getContext("2d");
	
	var map;
	var xmax,ymax;
	this.init = function(x_,y_) {
		xmax = x_;
		ymax = y_;
		map = new Array();
		for(i=1;i<=xmax;i++) {
			map[i] = new Array();
			for(j=1;j<=ymax;j++) {
				map[i][j] = -1;	
			}
		}
	}
	
	this.update = function() {
		for(i=1;i<=xmax;i++) {
			for(j=1;j<=ymax;j++) {
				if(map[i][j] == 1){
					draw.fillStyle = "rgb(0, 0, 255)";
					
				}else if(map[i][j] == 2) {
					draw.fillStyle = "rgb(255, 0, 0)";
					draw.fillRect(i * 3, j * 3, 3, 3);
				}else if(map[i][j] == 3) {
					draw.fillStyle = "rgb(0, 255, 0)";
					draw.fillRect(i * 3, j * 3, 3, 3);
				}else if(map[i][j] == 0) {
					draw.clearRect(i * 3, j * 3, 3, 3);	
				}
			}
		}
	}
	
    this.drawer = function(x_,y_, a) {
		map[x_][y_] = ((map[x_][y_]==0)||(map[x_][y_]==-1)) ? a : 0;
		if(map[x_][y_] == 1){
					draw.fillStyle = "rgb(0, 0, 255)";
					draw.fillRect(x_ * 5, y_ * 5, 5, 5);
				}else if(map[x_][y_] == 2) {
					draw.fillStyle = "rgb(255, 0, 0)";
					draw.fillRect(x_ * 5, y_ * 5, 5, 5);
				}else if(map[x_][y_] == 3) {
					draw.fillStyle = "rgb(0, 255, 0)";
					draw.fillRect(x_ * 5, y_ * 5, 5, 5);
				}else if(map[x_][y_] == 0) {
					draw.clearRect(x_ * 5, y_ * 5, 5, 5);	
				}
	}
	this.get_map = function(x_,y_) {
		return map[x_][y_];	
	}
}