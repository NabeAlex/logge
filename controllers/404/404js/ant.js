// JavaScript Document
function Ant(x_ , y_ , vector_ , max_) {
	this.x = x_;
	this.y = y_;
	var begin_x = x_;
	var begin_y = y_;
	var vector = 2;
	var maxi = max_;
	this.move = function(point) {
		if(vector == 1) {
			vector = (point == 0) ? 2 : 4;
		}else if(vector == 2) {
			vector = (point == 0) ? 3 : 1;
		}else if(vector == 3) {
			vector = (point == 0) ? 4 : 2;
		}else{
			vector = (point == 0) ? 1 : 3;
		}
		
	
		if((this.x == 1)||(this.x == (maxi - 1))) {
			this.x = begin_x;
			this.y = begin_y; 
		}
		
		if((this.y == 1)||(this.y == (maxi - 1))) {
			vector += ((vector==1)||(vector==3)) ? 1 : (-1);	
			if(vector==0) 
				vector = 2;		
		}
	}
	
	this.toForward = function() {
		if(vector == 1) {
			this.y--;	
		}else if(vector == 2) {
			this.x++;
		}else if(vector == 3) {
			this.y++;
		}else{
			this.x--;	
		}
	}
	this.getX = function() {
		return x;	
	}
	this.getY = function() {
		return y;
	}
}