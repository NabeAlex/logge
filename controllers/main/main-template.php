<style>
	#hint {
		font-family:Georgia, "Times New Roman", Times, serif;
		font-size:12px;
		font-style:italic;
		vertical-align:central;
		display:none;
		z-index:50;
		width:100%;
		display:none;
		background:#FFF;
	}
	.hints {
		margin-top:2px;
		z-index:30;
		position:absolute;
		text-align:left;
		cursor:pointer;
		background:#FFF;
		min-width:20%;
		padding-top:3px;
		padding-bottom:-4px;
		height:20px;
		border-radius:2px;
		display:block;
		
	}
	.hint-select {
		margin-top:2px;
		z-index:30;
		position:absolute;
		text-align:left;
		cursor:pointer;
		background:#A0A0A4;
		min-width:20%;
		padding-top:3px;
		padding-bottom:-4px;
		height:20px;
		border-radius:2px;
		display:block;
		
	}
	.hints:hover {
		background:#CCC;
	}
	.link-hint {
		z-index:60;
		font-size:90%;
		text-decoration:none;
		color:#333;	
	}
</style>
<div id="main">
        <div class="holder">
    		<div id="header-nav">
            	<div id="head_text"></div>
            	<input autocomplete="off" aria-haspopup="false" role="combobox" spellcheck="false" type="text" dir="ltr" aria-grabbed="false" id="company" <?=((isset($_GET['company'])&&($_GET['company']!="")))?"":'autofocus="autofocus"'?> value="<?=$_GET["company"];?>" maxlength="20" />
                <!--<div id="clear"></div>-->
                <div id="hint"></div>
                <input class="gogogo" style="margin-top:5px;" type="submit" value="Go" />
            </div>
        </div>
        </div>