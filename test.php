<!DOCTYPE HTML>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <style type="text/css">
    *{padding:0;margin:0;}
    html,body{
        width: 100%;
        height: 100%;
        background: rgb(157,213,58);
    }
	canvas{
		cursor: crosshair;
		position: relative;
	}
	#fixed{
		position: fixed;
		right: 0;
		top: 0;
		background: rgba(222,222,244,.6);
		border: 5px solid #000;
		z-index:2;
	}
    </style>
</head>
<body>
 
<script type="text/javascript">
function id(i){return document.getElementById(i)}
function read(file) {
    var d = document,
        canvas = d.createElement('canvas'),
        ctx = canvas.getContext('2d'),
		img = new Image(),
		out = id('output');
		
    img.onload = function() {
		canvas.width = img.width;
		canvas.height = img.height;
        
		ctx.drawImage(img, 0, 0); 
		
		var data = ctx.getImageData(0, 0, canvas.width, canvas.height).data;
		
		canvas.onmousemove = function(e){
			var i = (e.layerX + e.layerY * canvas.width) * 4;
			
			out.innerHTML = 
			'x: ' + e.layerX +
			'<br/>y: ' + e.layerY +
			'<br/>r: ' + data[i] +
			'<br/>g: ' + data[i+1] +
			'<br/>b: ' + data[i+2] +
			'<br/>a: ' + data[i+3];
			out.parentNode.style.borderColor = 'rgba(' + data[i] + ',' + data[i+1] + ',' + data[i+2] + ',' + data[i+3] + ')';
		};
		
		d.body.appendChild(canvas);
    }
    img.src = URL.createObjectURL( file );
    
}
</script>
<div id="fixed" >
	<input type="file" id="input" onchange="read(files[0])"/>
	<div id="output"/></div>
</div>
</body>
</html>

