﻿<?php
	require_once("../modules/image/hex.php");
	require_once("../modules/image/compare.php");
	require_once("../system.php");
	$img = $_POST['img'];
	$file = explode("." , $_POST['file']);
	$file = $file[count($file) - 1];
	$file = md5(mt_rand(1,10000)).".".$file;
	$img = str_replace('data:image/png;base64,' , '' , $img);
	$img = str_replace(' ' , '+' , $img);
	$echo = work::PutFile(array("FILE" => $file , "TYPES" => array("png" , "jpg"), "URL" => "images_checks" ,
					"VALUE" => $img) , true);
	if(!($echo)) { echo "error" ;  exit(); }
	
	///////////////////////
	////////Search/////////
	///////////////////////
	
	$hex = new hex("images_checks/".$echo["NAME"].".".$echo["TYPE"] , $echo["TYPE"]);
	$hex_main = $hex->get();
	compare::$path = "../images_json/*.json";
	$compare = new compare($hex_main);
	$result = $compare->get();
	foreach($result as $part) {
		echo $part["NAME"]["NAME"]." : ".($part["VALUE"]*100)."%<br>";	
	}
	
	/********
	
		json = json_encode($hex->get());
		file_put_contents("json/".$echo["NAME"].".json" , $json);
		
	*********/
?>